import React from "react";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";

import Routes from "./routes";

function App() {
  return (
    <div>
      <Routes />
    </div>
  );
}

export default App;
