import axios from "axios";

const url = process.env.API_ENDPOINT;
console.log(url);

const api = axios.create({
  baseURL: url ?? "https://backend-simplon.herokuapp.com",
});

export default api;
