import React, { useEffect, useState } from "react";
import { FiLogOut, FiArrowRight } from "react-icons/fi";
import { Link, useHistory } from "react-router-dom";
import jwtDecode from "jwt-decode";

import api from "../../services/api";
import notify from "../../utils";

import "./styles.css";

function Dashboard() {
  const history = useHistory();
  const [email, setEmail] = useState("User");

  useEffect(async () => {
    const token = localStorage.getItem("token");
    const user = token ? jwtDecode(token) : {};
    if (!user.email) {
      notify.error("Your session is invalid.");
      localStorage.removeItem("token");
      history.push("/");
    } else {
      setEmail(user.email);
      await api
        .post("/verify-token", null, {
          headers: { authorization: `Bearer ${token}` },
        })
        .catch(() => {
          localStorage.removeItem("token");
          notify.error("Your session is invalid.");
          history.push("/");
        });
    }
  }, []);

  function handleLogOut() {
    localStorage.removeItem("token");
    history.push("/");
  }

  return (
    <div id="dashboard">
      <div className="content">
        <header>
          <Link to="/update-password">
            <FiArrowRight />
            Update password
          </Link>
        </header>

        <main>
          <h1>Bienvenue, {email}</h1>
          <p>Nous vous aidons à faire quelque chose...</p>

          <Link onClick={handleLogOut} id="home" to="/">
            <span>
              <FiLogOut />
            </span>
            <strong>Log out.</strong>
          </Link>
        </main>
      </div>
    </div>
  );
}

export default Dashboard;
