import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";

import api from "../../services/api";
import notify from "../../utils";

import "./styles.css";

function ForgotPassword() {
  const history = useHistory();
  const [email, setEmail] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();

    await api
      .post("/get-new-password", { email })
      .then((response) => {
        history.push("/login");
        notify.success(response.data.message);
      })
      .catch((error) => {
        notify.error(error.response.data.error);
      });
  }

  return (
    <div id="forgot-password">
      <header>
        <Link to="/login">
          <FiArrowLeft />
          Login page
        </Link>
      </header>

      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>
            <h2>Get new password</h2>
          </legend>

          <div className="field">
            <label htmlFor="email">E-mail</label>
            <input
              type="email"
              name="email"
              id="email"
              value={email}
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </div>
        </fieldset>
        <button type="submit">Send</button>
      </form>
    </div>
  );
}

export default ForgotPassword;
