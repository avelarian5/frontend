import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowLeft, FiArrowRightCircle } from "react-icons/fi";

import api from "../../services/api";
import notify from "../../utils";

import "./styles.css";

function Login() {
  const history = useHistory();
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  function handleInputChange(event) {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  }

  async function handleSubmit(event) {
    event.preventDefault();

    await api
      .post("/login", formData)
      .then((response) => {
        localStorage.setItem("token", response.data.token);
        history.push("/dashboard");
        notify.success(response.data.message);
      })
      .catch((error) => {
        notify.error(error.response.data.error);
      });
  }

  return (
    <div id="login">
      <header>
        <Link to="/">
          <FiArrowLeft />
          Home page
        </Link>
      </header>

      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>
            <h2>Login</h2>
          </legend>

          <div className="field">
            <label htmlFor="email">E-mail</label>
            <input
              type="email"
              name="email"
              id="email"
              value={formData.email}
              onChange={handleInputChange}
            />
          </div>

          <div className="field">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              id="password"
              value={formData.password}
              onChange={handleInputChange}
            />
          </div>

          <Link to="/forgot-password">
            Forgot password?
            <FiArrowRightCircle />
          </Link>
        </fieldset>
        <button type="submit">Enter</button>
      </form>
    </div>
  );
}

export default Login;
