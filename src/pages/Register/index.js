import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";

import api from "../../services/api";
import notify from "../../utils";

import "./styles.css";

function Register() {
  const history = useHistory();
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  function handleInputChange(event) {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  }

  async function handleSubmit(event) {
    event.preventDefault();

    await api
      .post("/register", formData)
      .then((response) => {
        notify.success(response.data.message);
        history.push("/");
      })
      .catch((error) => {
        notify.error(error.response.data.error);
      });
  }

  return (
    <div id="register">
      <header>
        <Link to="/">
          <FiArrowLeft />
          Home page
        </Link>
      </header>

      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>
            <h2>Register</h2>
          </legend>

          <div className="field">
            <label htmlFor="email">E-mail</label>
            <input
              type="email"
              name="email"
              id="email"
              value={formData.email}
              onChange={handleInputChange}
            />
          </div>

          <div className="field">
            <label htmlFor="email">Password</label>
            <input
              type="password"
              name="password"
              id="password"
              value={formData.password}
              onChange={handleInputChange}
            />
          </div>
        </fieldset>
        <button type="submit">Confirm</button>
      </form>
    </div>
  );
}

export default Register;
