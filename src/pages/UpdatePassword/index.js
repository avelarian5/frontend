import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import jwtDecode from "jwt-decode";

import api from "../../services/api";
import notify from "../../utils";

import "./styles.css";

function GetNewPassword() {
  const history = useHistory();
  const token = localStorage.getItem("token");
  const user = token ? jwtDecode(token) : {};
  const [formData, setFormData] = useState({
    newPassword: "",
    oldPassword: "",
  });

  function handleInputChange(event) {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  }

  async function handleSubmit(event) {
    event.preventDefault();

    await api
      .post(`/update-password/${user.id}`, formData, {
        headers: { authorization: `Bearer ${localStorage.getItem("token")}` },
      })
      .then((response) => {
        history.push("/dashboard");
        notify.success(response.data.message);
      })
      .catch((error) => {
        notify.error(error.response.data.error);
      });
  }

  useEffect(async () => {
    if (!user.email) {
      notify.error("Your session is invalid.");
      localStorage.removeItem("token");
      history.push("/");
    } else {
      await api
        .post("/verify-token", null, {
          headers: { authorization: `Bearer ${token}` },
        })
        .catch(() => {
          localStorage.removeItem("token");
          notify.error("Your session is invalid.");
          history.push("/");
        });
    }
  }, []);

  return (
    <div id="get-new-password">
      <header>
        <Link to="/dashboard">
          <FiArrowLeft />
          Dashboard page
        </Link>
      </header>

      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>
            <h2>Update password</h2>
          </legend>

          <div className="field">
            <label htmlFor="password">Old Password</label>
            <input
              type="password"
              name="oldPassword"
              id="oldPassword"
              value={formData.oldPassword}
              onChange={handleInputChange}
            />
          </div>

          <div className="field">
            <label htmlFor="password">New Password</label>
            <input
              type="password"
              name="newPassword"
              id="newPassword"
              value={formData.newPassword}
              onChange={handleInputChange}
            />
          </div>
        </fieldset>
        <button type="submit">Update</button>
      </form>
    </div>
  );
}

export default GetNewPassword;
