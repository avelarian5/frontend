import React from "react";
import { FiLogIn, FiEdit } from "react-icons/fi";
import { Link } from "react-router-dom";

import "./styles.css";

function Home() {
  return (
    <div id="home">
      <div className="content">
        <main>
          <h1>Our system</h1>
          <p>We help you do something...</p>

          <Link id="login" to="/login">
            <span>
              <FiLogIn />
            </span>
            <strong>Log in.</strong>
          </Link>

          <Link id="register" to="/register">
            <span>
              <FiEdit />
            </span>
            <strong>Register.</strong>
          </Link>
        </main>
      </div>
    </div>
  );
}

export default Home;
