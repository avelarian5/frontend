import React from "react";
import { Route, BrowserRouter } from "react-router-dom";

import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import Register from "./pages/Register";
import Login from "./pages/Login";
import ForgotPassword from "./pages/ForgotPassword";
import UpdatePassword from "./pages/UpdatePassword";

function Routes() {
  return (
    <BrowserRouter>
      <Route component={Home} path="/" exact />
      <Route component={Dashboard} path="/dashboard" exact />
      <Route component={Register} path="/register" />
      <Route component={Login} path="/login" />
      <Route component={ForgotPassword} path="/forgot-password" />
      <Route component={UpdatePassword} path="/update-password" />
    </BrowserRouter>
  );
}

export default Routes;
